// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

internal class TravelexpenseServiceFactory {
    static func registerAll() throws {
        TravelexpenseServiceMetadata.EntityTypes.attachment.registerFactory(ObjectFactory.with(create: { Attachment(withDefaults: false) }, createWithDecoder: { d in try Attachment(from: d) }))
        TravelexpenseServiceMetadata.EntityTypes.currency.registerFactory(ObjectFactory.with(create: { Currency(withDefaults: false) }, createWithDecoder: { d in try Currency(from: d) }))
        TravelexpenseServiceMetadata.EntityTypes.expense.registerFactory(ObjectFactory.with(create: { Expense(withDefaults: false) }, createWithDecoder: { d in try Expense(from: d) }))
        TravelexpenseServiceMetadata.EntityTypes.expenseType.registerFactory(ObjectFactory.with(create: { ExpenseType(withDefaults: false) }, createWithDecoder: { d in try ExpenseType(from: d) }))
        TravelexpenseServiceMetadata.EntityTypes.paymentType.registerFactory(ObjectFactory.with(create: { PaymentType(withDefaults: false) }, createWithDecoder: { d in try PaymentType(from: d) }))
        TravelexpenseServiceMetadata.EntityTypes.report.registerFactory(ObjectFactory.with(create: { Report(withDefaults: false) }, createWithDecoder: { d in try Report(from: d) }))
        TravelexpenseServiceMetadata.EntityTypes.reportStatus.registerFactory(ObjectFactory.with(create: { ReportStatus(withDefaults: false) }, createWithDecoder: { d in try ReportStatus(from: d) }))
        TravelexpenseServiceStaticResolver.resolve()
    }
}
