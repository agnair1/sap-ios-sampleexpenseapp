// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

internal class TravelexpenseServiceMetadataParser {
    internal static let options: Int = (CSDLOption.allowCaseConflicts | CSDLOption.disableFacetWarnings | CSDLOption.disableNameValidation | CSDLOption.processMixedVersions | CSDLOption.retainOriginalText | CSDLOption.ignoreUndefinedTerms)

    internal static let parsed: CSDLDocument = TravelexpenseServiceMetadataParser.parse()

    static func parse() -> CSDLDocument {
        let parser = CSDLParser()
        parser.logWarnings = false
        parser.csdlOptions = TravelexpenseServiceMetadataParser.options
        let metadata = parser.parseInProxy(TravelexpenseServiceMetadataText.xml, url: "com.example.backend.travelexpense")
        metadata.proxyVersion = "19.1.4-aa99e2-20190611"
        return metadata
    }
}
