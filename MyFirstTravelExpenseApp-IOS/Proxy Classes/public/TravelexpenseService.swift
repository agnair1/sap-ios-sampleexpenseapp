// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

open class TravelexpenseService<Provider: DataServiceProvider>: DataService<Provider> {
    public override init(provider: Provider) {
        super.init(provider: provider)
        self.provider.metadata = TravelexpenseServiceMetadata.document
    }

    @available(swift, deprecated: 4.0, renamed: "fetchAttachmentSet")
    open func attachmentSet(query: DataQuery = DataQuery()) throws -> [Attachment] {
        return try self.fetchAttachmentSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchAttachmentSet")
    open func attachmentSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Attachment]?, Error?) -> Void) {
        self.fetchAttachmentSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchCurrencySet")
    open func currencySet(query: DataQuery = DataQuery()) throws -> [Currency] {
        return try self.fetchCurrencySet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchCurrencySet")
    open func currencySet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Currency]?, Error?) -> Void) {
        self.fetchCurrencySet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchExpenseSet")
    open func expenseSet(query: DataQuery = DataQuery()) throws -> [Expense] {
        return try self.fetchExpenseSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchExpenseSet")
    open func expenseSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Expense]?, Error?) -> Void) {
        self.fetchExpenseSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchExpenseTypeSet")
    open func expenseTypeSet(query: DataQuery = DataQuery()) throws -> [ExpenseType] {
        return try self.fetchExpenseTypeSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchExpenseTypeSet")
    open func expenseTypeSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([ExpenseType]?, Error?) -> Void) {
        self.fetchExpenseTypeSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    open func fetchAttachment(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Attachment {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Attachment>.from(self.executeQuery(query.fromDefault(TravelexpenseServiceMetadata.EntitySets.attachmentSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAttachment(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Attachment?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAttachment(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAttachmentSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Attachment] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Attachment.array(from: self.executeQuery(var_query.fromDefault(TravelexpenseServiceMetadata.EntitySets.attachmentSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAttachmentSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Attachment]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAttachmentSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAttachmentWithKey(attachmentID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Attachment {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAttachment(matching: var_query.withKey(Attachment.key(attachmentID: attachmentID)), headers: headers, options: options)
    }

    open func fetchAttachmentWithKey(attachmentID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Attachment?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAttachmentWithKey(attachmentID: attachmentID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCurrency(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Currency {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Currency>.from(self.executeQuery(query.fromDefault(TravelexpenseServiceMetadata.EntitySets.currencySet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchCurrency(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Currency?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCurrency(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCurrencySet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Currency] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Currency.array(from: self.executeQuery(var_query.fromDefault(TravelexpenseServiceMetadata.EntitySets.currencySet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchCurrencySet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Currency]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCurrencySet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCurrencyWithKey(currencyID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Currency {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchCurrency(matching: var_query.withKey(Currency.key(currencyID: currencyID)), headers: headers, options: options)
    }

    open func fetchCurrencyWithKey(currencyID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Currency?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCurrencyWithKey(currencyID: currencyID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchExpense(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Expense {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Expense>.from(self.executeQuery(query.fromDefault(TravelexpenseServiceMetadata.EntitySets.expenseSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchExpense(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Expense?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchExpense(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchExpenseSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Expense] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Expense.array(from: self.executeQuery(var_query.fromDefault(TravelexpenseServiceMetadata.EntitySets.expenseSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchExpenseSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Expense]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchExpenseSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchExpenseType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ExpenseType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<ExpenseType>.from(self.executeQuery(query.fromDefault(TravelexpenseServiceMetadata.EntitySets.expenseTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchExpenseType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ExpenseType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchExpenseType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchExpenseTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [ExpenseType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ExpenseType.array(from: self.executeQuery(var_query.fromDefault(TravelexpenseServiceMetadata.EntitySets.expenseTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchExpenseTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([ExpenseType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchExpenseTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchExpenseTypeWithKey(expenseTypeID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ExpenseType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchExpenseType(matching: var_query.withKey(ExpenseType.key(expenseTypeID: expenseTypeID)), headers: headers, options: options)
    }

    open func fetchExpenseTypeWithKey(expenseTypeID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ExpenseType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchExpenseTypeWithKey(expenseTypeID: expenseTypeID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchExpenseWithKey(expenseID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Expense {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchExpense(matching: var_query.withKey(Expense.key(expenseID: expenseID)), headers: headers, options: options)
    }

    open func fetchExpenseWithKey(expenseID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Expense?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchExpenseWithKey(expenseID: expenseID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchPaymentType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> PaymentType {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<PaymentType>.from(self.executeQuery(query.fromDefault(TravelexpenseServiceMetadata.EntitySets.paymentTypeSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchPaymentType(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (PaymentType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchPaymentType(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchPaymentTypeSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [PaymentType] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try PaymentType.array(from: self.executeQuery(var_query.fromDefault(TravelexpenseServiceMetadata.EntitySets.paymentTypeSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchPaymentTypeSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([PaymentType]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchPaymentTypeSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchPaymentTypeWithKey(paymentTypeID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> PaymentType {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchPaymentType(matching: var_query.withKey(PaymentType.key(paymentTypeID: paymentTypeID)), headers: headers, options: options)
    }

    open func fetchPaymentTypeWithKey(paymentTypeID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (PaymentType?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchPaymentTypeWithKey(paymentTypeID: paymentTypeID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchReport(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Report {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Report>.from(self.executeQuery(query.fromDefault(TravelexpenseServiceMetadata.EntitySets.reportSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchReport(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Report?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchReport(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchReportSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Report] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Report.array(from: self.executeQuery(var_query.fromDefault(TravelexpenseServiceMetadata.EntitySets.reportSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchReportSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Report]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchReportSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchReportStatus(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ReportStatus {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<ReportStatus>.from(self.executeQuery(query.fromDefault(TravelexpenseServiceMetadata.EntitySets.reportStatusSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchReportStatus(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ReportStatus?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchReportStatus(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchReportStatusSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [ReportStatus] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ReportStatus.array(from: self.executeQuery(var_query.fromDefault(TravelexpenseServiceMetadata.EntitySets.reportStatusSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchReportStatusSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([ReportStatus]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchReportStatusSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchReportStatusWithKey(reportStatusID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ReportStatus {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchReportStatus(matching: var_query.withKey(ReportStatus.key(reportStatusID: reportStatusID)), headers: headers, options: options)
    }

    open func fetchReportStatusWithKey(reportStatusID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ReportStatus?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchReportStatusWithKey(reportStatusID: reportStatusID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchReportWithKey(reportID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Report {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchReport(matching: var_query.withKey(Report.key(reportID: reportID)), headers: headers, options: options)
    }

    open func fetchReportWithKey(reportID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Report?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchReportWithKey(reportID: reportID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    @available(swift, deprecated: 4.0, renamed: "fetchPaymentTypeSet")
    open func paymentTypeSet(query: DataQuery = DataQuery()) throws -> [PaymentType] {
        return try self.fetchPaymentTypeSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchPaymentTypeSet")
    open func paymentTypeSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([PaymentType]?, Error?) -> Void) {
        self.fetchPaymentTypeSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    open override func refreshMetadata() throws {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        do {
            try ProxyInternal.refreshMetadata(service: self, fetcher: nil, options: TravelexpenseServiceMetadataParser.options)
            TravelexpenseServiceMetadataChanges.merge(metadata: self.metadata)
        }
    }

    @available(swift, deprecated: 4.0, renamed: "fetchReportSet")
    open func reportSet(query: DataQuery = DataQuery()) throws -> [Report] {
        return try self.fetchReportSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchReportSet")
    open func reportSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Report]?, Error?) -> Void) {
        self.fetchReportSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchReportStatusSet")
    open func reportStatusSet(query: DataQuery = DataQuery()) throws -> [ReportStatus] {
        return try self.fetchReportStatusSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchReportStatusSet")
    open func reportStatusSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([ReportStatus]?, Error?) -> Void) {
        self.fetchReportStatusSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }
}
