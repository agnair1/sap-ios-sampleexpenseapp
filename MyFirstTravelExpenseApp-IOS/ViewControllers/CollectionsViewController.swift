//
// MyFirstTravelExpenseApp-IOS
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 18/09/19
//

import Foundation
import SAPFiori
import SAPFioriFlows
import SAPOData

protocol EntityUpdaterDelegate {
    func entityHasChanged(_ entity: EntityValue?)
}

protocol EntitySetUpdaterDelegate {
    func entitySetHasChanged()
}

class CollectionsViewController: FUIFormTableViewController {
    private var collections = CollectionType.all

    // Variable to store the selected index path
    private var selectedIndex: IndexPath?

    private let okTitle = NSLocalizedString("keyOkButtonTitle",
                                            value: "OK",
                                            comment: "XBUT: Title of OK button.")

    var isPresentedInSplitView: Bool {
        return !(self.splitViewController?.isCollapsed ?? true)
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredContentSize = CGSize(width: 320, height: 480)

        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.makeSelection()
    }

    override func viewWillTransition(to _: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: nil, completion: { _ in
            let isNotInSplitView = !self.isPresentedInSplitView
            self.tableView.visibleCells.forEach { cell in
                // To refresh the disclosure indicator of each cell
                cell.accessoryType = isNotInSplitView ? .disclosureIndicator : .none
            }
            self.makeSelection()
        })
    }

    // MARK: - UITableViewDelegate

    override func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return collections.count
    }

    override func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return 44
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FUIObjectTableViewCell.reuseIdentifier, for: indexPath) as! FUIObjectTableViewCell
        cell.headlineLabel.text = self.collections[indexPath.row].rawValue
        cell.accessoryType = !self.isPresentedInSplitView ? .disclosureIndicator : .none
        cell.isMomentarySelection = false
        return cell
    }

    override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.collectionSelected(at: indexPath)
    }

    // CollectionType selection helper
    private func collectionSelected(at indexPath: IndexPath) {
        // Load the EntityType specific ViewController from the specific storyboard"
        var masterViewController: UIViewController!
        guard let travelexpenseService = OnboardingSessionManager.shared.onboardingSession?.odataController.travelexpenseService else {
            AlertHelper.displayAlert(with: "OData service is not reachable, please onboard again.", error: nil, viewController: self)
            return
        }
        self.selectedIndex = indexPath

        switch self.collections[indexPath.row] {
        case .currencySet:
            let currencyStoryBoard = UIStoryboard(name: "Currency", bundle: nil)
            let currencyMasterViewController = currencyStoryBoard.instantiateViewController(withIdentifier: "CurrencyMaster") as! CurrencyMasterViewController
            currencyMasterViewController.travelexpenseService = travelexpenseService
            currencyMasterViewController.entitySetName = "CurrencySet"
            func fetchCurrencySet(_ completionHandler: @escaping ([Currency]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    travelexpenseService.fetchCurrencySet(matching: query, completionHandler: completionHandler)
                }
            }
            currencyMasterViewController.loadEntitiesBlock = fetchCurrencySet
            currencyMasterViewController.navigationItem.title = "Currency"
            masterViewController = currencyMasterViewController
        case .expenseSet:
            let expenseStoryBoard = UIStoryboard(name: "Expense", bundle: nil)
            let expenseMasterViewController = expenseStoryBoard.instantiateViewController(withIdentifier: "ExpenseMaster") as! ExpenseMasterViewController
            expenseMasterViewController.travelexpenseService = travelexpenseService
            expenseMasterViewController.entitySetName = "ExpenseSet"
            func fetchExpenseSet(_ completionHandler: @escaping ([Expense]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    travelexpenseService.fetchExpenseSet(matching: query, completionHandler: completionHandler)
                }
            }
            expenseMasterViewController.loadEntitiesBlock = fetchExpenseSet
            expenseMasterViewController.navigationItem.title = "Expense"
            masterViewController = expenseMasterViewController
        case .reportSet:
            let reportStoryBoard = UIStoryboard(name: "Report", bundle: nil)
            let reportMasterViewController = reportStoryBoard.instantiateViewController(withIdentifier: "ReportMaster") as! ReportMasterViewController
            reportMasterViewController.travelexpenseService = travelexpenseService
            reportMasterViewController.entitySetName = "ReportSet"
            func fetchReportSet(_ completionHandler: @escaping ([Report]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    travelexpenseService.fetchReportSet(matching: query, completionHandler: completionHandler)
                }
            }
            reportMasterViewController.loadEntitiesBlock = fetchReportSet
            reportMasterViewController.navigationItem.title = "Report"
            masterViewController = reportMasterViewController
        case .expenseTypeSet:
            let expenseTypeStoryBoard = UIStoryboard(name: "ExpenseType", bundle: nil)
            let expenseTypeMasterViewController = expenseTypeStoryBoard.instantiateViewController(withIdentifier: "ExpenseTypeMaster") as! ExpenseTypeMasterViewController
            expenseTypeMasterViewController.travelexpenseService = travelexpenseService
            expenseTypeMasterViewController.entitySetName = "ExpenseTypeSet"
            func fetchExpenseTypeSet(_ completionHandler: @escaping ([ExpenseType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    travelexpenseService.fetchExpenseTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            expenseTypeMasterViewController.loadEntitiesBlock = fetchExpenseTypeSet
            expenseTypeMasterViewController.navigationItem.title = "ExpenseType"
            masterViewController = expenseTypeMasterViewController
        case .attachmentSet:
            let attachmentStoryBoard = UIStoryboard(name: "Attachment", bundle: nil)
            let attachmentMasterViewController = attachmentStoryBoard.instantiateViewController(withIdentifier: "AttachmentMaster") as! AttachmentMasterViewController
            attachmentMasterViewController.travelexpenseService = travelexpenseService
            attachmentMasterViewController.entitySetName = "AttachmentSet"
            func fetchAttachmentSet(_ completionHandler: @escaping ([Attachment]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    travelexpenseService.fetchAttachmentSet(matching: query, completionHandler: completionHandler)
                }
            }
            attachmentMasterViewController.loadEntitiesBlock = fetchAttachmentSet
            attachmentMasterViewController.navigationItem.title = "Attachment"
            masterViewController = attachmentMasterViewController
        case .reportStatusSet:
            let reportStatusStoryBoard = UIStoryboard(name: "ReportStatus", bundle: nil)
            let reportStatusMasterViewController = reportStatusStoryBoard.instantiateViewController(withIdentifier: "ReportStatusMaster") as! ReportStatusMasterViewController
            reportStatusMasterViewController.travelexpenseService = travelexpenseService
            reportStatusMasterViewController.entitySetName = "ReportStatusSet"
            func fetchReportStatusSet(_ completionHandler: @escaping ([ReportStatus]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    travelexpenseService.fetchReportStatusSet(matching: query, completionHandler: completionHandler)
                }
            }
            reportStatusMasterViewController.loadEntitiesBlock = fetchReportStatusSet
            reportStatusMasterViewController.navigationItem.title = "ReportStatus"
            masterViewController = reportStatusMasterViewController
        case .paymentTypeSet:
            let paymentTypeStoryBoard = UIStoryboard(name: "PaymentType", bundle: nil)
            let paymentTypeMasterViewController = paymentTypeStoryBoard.instantiateViewController(withIdentifier: "PaymentTypeMaster") as! PaymentTypeMasterViewController
            paymentTypeMasterViewController.travelexpenseService = travelexpenseService
            paymentTypeMasterViewController.entitySetName = "PaymentTypeSet"
            func fetchPaymentTypeSet(_ completionHandler: @escaping ([PaymentType]?, Error?) -> Void) {
                // Only request the first 20 values. If you want to modify the requested entities, you can do it here.
                let query = DataQuery().selectAll().top(20)
                do {
                    travelexpenseService.fetchPaymentTypeSet(matching: query, completionHandler: completionHandler)
                }
            }
            paymentTypeMasterViewController.loadEntitiesBlock = fetchPaymentTypeSet
            paymentTypeMasterViewController.navigationItem.title = "PaymentType"
            masterViewController = paymentTypeMasterViewController
        case .none:
            masterViewController = UIViewController()
        }

        // Load the NavigationController and present with the EntityType specific ViewController
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let rightNavigationController = mainStoryBoard.instantiateViewController(withIdentifier: "RightNavigationController") as! UINavigationController
        rightNavigationController.viewControllers = [masterViewController]
        self.splitViewController?.showDetailViewController(rightNavigationController, sender: nil)
    }

    // MARK: - Handle highlighting of selected cell

    private func makeSelection() {
        if let selectedIndex = selectedIndex {
            tableView.selectRow(at: selectedIndex, animated: true, scrollPosition: .none)
            tableView.scrollToRow(at: selectedIndex, at: .none, animated: true)
        } else {
            selectDefault()
        }
    }

    private func selectDefault() {
        // Automatically select first element if we have two panels (iPhone plus and iPad only)
        if self.splitViewController!.isCollapsed || OnboardingSessionManager.shared.onboardingSession?.odataController.travelexpenseService == nil {
            return
        }
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        self.collectionSelected(at: indexPath)
    }
}
