// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

open class Report: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var city_: Property = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "City")

    private static var costCenter_: Property = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "CostCenter")

    private static var country_: Property = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "Country")

    private static var end_: Property = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "End")

    private static var name_: Property = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "Name")

    private static var reportID_: Property = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "ReportID")

    private static var reportStatusID_: Property = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "ReportStatusID")

    private static var start_: Property = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "Start")

    private static var state_: Property = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "State")

    private static var expenses_: Property = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "Expenses")

    private static var reportStatus_: Property = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "ReportStatus")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: TravelexpenseServiceMetadata.EntityTypes.report)
    }

    open class func array(from: EntityValueList) -> [Report] {
        return ArrayConverter.convert(from.toArray(), [Report]())
    }

    open class var city: Property {
        get {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                return Report.city_
            }
        }
        set(value) {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                Report.city_ = value
            }
        }
    }

    open var city: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Report.city))
        }
        set(value) {
            self.setOptionalValue(for: Report.city, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> Report {
        return CastRequired<Report>.from(self.copyEntity())
    }

    open class var costCenter: Property {
        get {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                return Report.costCenter_
            }
        }
        set(value) {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                Report.costCenter_ = value
            }
        }
    }

    open var costCenter: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Report.costCenter))
        }
        set(value) {
            self.setOptionalValue(for: Report.costCenter, to: StringValue.of(optional: value))
        }
    }

    open class var country: Property {
        get {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                return Report.country_
            }
        }
        set(value) {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                Report.country_ = value
            }
        }
    }

    open var country: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Report.country))
        }
        set(value) {
            self.setOptionalValue(for: Report.country, to: StringValue.of(optional: value))
        }
    }

    open class var end: Property {
        get {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                return Report.end_
            }
        }
        set(value) {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                Report.end_ = value
            }
        }
    }

    open var end: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: Report.end))
        }
        set(value) {
            self.setOptionalValue(for: Report.end, to: value)
        }
    }

    open class var expenses: Property {
        get {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                return Report.expenses_
            }
        }
        set(value) {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                Report.expenses_ = value
            }
        }
    }

    open var expenses: [Expense] {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: Report.expenses)).toArray(), [Expense]())
        }
        set(value) {
            Report.expenses.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(reportID: Int64?) -> EntityKey {
        return EntityKey().with(name: "ReportID", value: LongValue.of(optional: reportID))
    }

    open class var name: Property {
        get {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                return Report.name_
            }
        }
        set(value) {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                Report.name_ = value
            }
        }
    }

    open var name: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Report.name))
        }
        set(value) {
            self.setOptionalValue(for: Report.name, to: StringValue.of(optional: value))
        }
    }

    open var old: Report {
        return CastRequired<Report>.from(self.oldEntity)
    }

    open class var reportID: Property {
        get {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                return Report.reportID_
            }
        }
        set(value) {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                Report.reportID_ = value
            }
        }
    }

    open var reportID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: Report.reportID))
        }
        set(value) {
            self.setOptionalValue(for: Report.reportID, to: LongValue.of(optional: value))
        }
    }

    open class var reportStatus: Property {
        get {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                return Report.reportStatus_
            }
        }
        set(value) {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                Report.reportStatus_ = value
            }
        }
    }

    open var reportStatus: ReportStatus? {
        get {
            return CastOptional<ReportStatus>.from(self.optionalValue(for: Report.reportStatus))
        }
        set(value) {
            self.setOptionalValue(for: Report.reportStatus, to: value)
        }
    }

    open class var reportStatusID: Property {
        get {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                return Report.reportStatusID_
            }
        }
        set(value) {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                Report.reportStatusID_ = value
            }
        }
    }

    open var reportStatusID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: Report.reportStatusID))
        }
        set(value) {
            self.setOptionalValue(for: Report.reportStatusID, to: LongValue.of(optional: value))
        }
    }

    open class var start: Property {
        get {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                return Report.start_
            }
        }
        set(value) {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                Report.start_ = value
            }
        }
    }

    open var start: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: Report.start))
        }
        set(value) {
            self.setOptionalValue(for: Report.start, to: value)
        }
    }

    open class var state: Property {
        get {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                return Report.state_
            }
        }
        set(value) {
            objc_sync_enter(Report.self)
            defer { objc_sync_exit(Report.self) }
            do {
                Report.state_ = value
            }
        }
    }

    open var state: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Report.state))
        }
        set(value) {
            self.setOptionalValue(for: Report.state, to: StringValue.of(optional: value))
        }
    }
}
