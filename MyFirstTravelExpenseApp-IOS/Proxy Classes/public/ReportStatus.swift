// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

open class ReportStatus: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var description_: Property = TravelexpenseServiceMetadata.EntityTypes.reportStatus.property(withName: "Description")

    private static var reportStatusID_: Property = TravelexpenseServiceMetadata.EntityTypes.reportStatus.property(withName: "ReportStatusID")

    private static var reports_: Property = TravelexpenseServiceMetadata.EntityTypes.reportStatus.property(withName: "Reports")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: TravelexpenseServiceMetadata.EntityTypes.reportStatus)
    }

    open class func array(from: EntityValueList) -> [ReportStatus] {
        return ArrayConverter.convert(from.toArray(), [ReportStatus]())
    }

    open func copy() -> ReportStatus {
        return CastRequired<ReportStatus>.from(self.copyEntity())
    }

    open class var description: Property {
        get {
            objc_sync_enter(ReportStatus.self)
            defer { objc_sync_exit(ReportStatus.self) }
            do {
                return ReportStatus.description_
            }
        }
        set(value) {
            objc_sync_enter(ReportStatus.self)
            defer { objc_sync_exit(ReportStatus.self) }
            do {
                ReportStatus.description_ = value
            }
        }
    }

    open var description: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ReportStatus.description))
        }
        set(value) {
            self.setOptionalValue(for: ReportStatus.description, to: StringValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(reportStatusID: Int64?) -> EntityKey {
        return EntityKey().with(name: "ReportStatusID", value: LongValue.of(optional: reportStatusID))
    }

    open var old: ReportStatus {
        return CastRequired<ReportStatus>.from(self.oldEntity)
    }

    open class var reportStatusID: Property {
        get {
            objc_sync_enter(ReportStatus.self)
            defer { objc_sync_exit(ReportStatus.self) }
            do {
                return ReportStatus.reportStatusID_
            }
        }
        set(value) {
            objc_sync_enter(ReportStatus.self)
            defer { objc_sync_exit(ReportStatus.self) }
            do {
                ReportStatus.reportStatusID_ = value
            }
        }
    }

    open var reportStatusID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: ReportStatus.reportStatusID))
        }
        set(value) {
            self.setOptionalValue(for: ReportStatus.reportStatusID, to: LongValue.of(optional: value))
        }
    }

    open class var reports: Property {
        get {
            objc_sync_enter(ReportStatus.self)
            defer { objc_sync_exit(ReportStatus.self) }
            do {
                return ReportStatus.reports_
            }
        }
        set(value) {
            objc_sync_enter(ReportStatus.self)
            defer { objc_sync_exit(ReportStatus.self) }
            do {
                ReportStatus.reports_ = value
            }
        }
    }

    open var reports: [Report] {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: ReportStatus.reports)).toArray(), [Report]())
        }
        set(value) {
            ReportStatus.reports.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }
}
