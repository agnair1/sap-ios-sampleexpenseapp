// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

internal class TravelexpenseServiceMetadataChanges {
    static func merge(metadata: CSDLDocument) {
        metadata.hasGeneratedProxies = true
        TravelexpenseServiceMetadata.document = metadata
        TravelexpenseServiceMetadataChanges.merge1(metadata: metadata)
        try! TravelexpenseServiceFactory.registerAll()
    }

    private static func merge1(metadata: CSDLDocument) {
        Ignore.valueOf_any(metadata)
        if !TravelexpenseServiceMetadata.EntityTypes.attachment.isRemoved {
            TravelexpenseServiceMetadata.EntityTypes.attachment = metadata.entityType(withName: "com.example.backend.travelexpense.Attachment")
        }
        if !TravelexpenseServiceMetadata.EntityTypes.currency.isRemoved {
            TravelexpenseServiceMetadata.EntityTypes.currency = metadata.entityType(withName: "com.example.backend.travelexpense.Currency")
        }
        if !TravelexpenseServiceMetadata.EntityTypes.expense.isRemoved {
            TravelexpenseServiceMetadata.EntityTypes.expense = metadata.entityType(withName: "com.example.backend.travelexpense.Expense")
        }
        if !TravelexpenseServiceMetadata.EntityTypes.expenseType.isRemoved {
            TravelexpenseServiceMetadata.EntityTypes.expenseType = metadata.entityType(withName: "com.example.backend.travelexpense.ExpenseType")
        }
        if !TravelexpenseServiceMetadata.EntityTypes.paymentType.isRemoved {
            TravelexpenseServiceMetadata.EntityTypes.paymentType = metadata.entityType(withName: "com.example.backend.travelexpense.PaymentType")
        }
        if !TravelexpenseServiceMetadata.EntityTypes.report.isRemoved {
            TravelexpenseServiceMetadata.EntityTypes.report = metadata.entityType(withName: "com.example.backend.travelexpense.Report")
        }
        if !TravelexpenseServiceMetadata.EntityTypes.reportStatus.isRemoved {
            TravelexpenseServiceMetadata.EntityTypes.reportStatus = metadata.entityType(withName: "com.example.backend.travelexpense.ReportStatus")
        }
        if !TravelexpenseServiceMetadata.EntitySets.attachmentSet.isRemoved {
            TravelexpenseServiceMetadata.EntitySets.attachmentSet = metadata.entitySet(withName: "AttachmentSet")
        }
        if !TravelexpenseServiceMetadata.EntitySets.currencySet.isRemoved {
            TravelexpenseServiceMetadata.EntitySets.currencySet = metadata.entitySet(withName: "CurrencySet")
        }
        if !TravelexpenseServiceMetadata.EntitySets.expenseSet.isRemoved {
            TravelexpenseServiceMetadata.EntitySets.expenseSet = metadata.entitySet(withName: "ExpenseSet")
        }
        if !TravelexpenseServiceMetadata.EntitySets.expenseTypeSet.isRemoved {
            TravelexpenseServiceMetadata.EntitySets.expenseTypeSet = metadata.entitySet(withName: "ExpenseTypeSet")
        }
        if !TravelexpenseServiceMetadata.EntitySets.paymentTypeSet.isRemoved {
            TravelexpenseServiceMetadata.EntitySets.paymentTypeSet = metadata.entitySet(withName: "PaymentTypeSet")
        }
        if !TravelexpenseServiceMetadata.EntitySets.reportSet.isRemoved {
            TravelexpenseServiceMetadata.EntitySets.reportSet = metadata.entitySet(withName: "ReportSet")
        }
        if !TravelexpenseServiceMetadata.EntitySets.reportStatusSet.isRemoved {
            TravelexpenseServiceMetadata.EntitySets.reportStatusSet = metadata.entitySet(withName: "ReportStatusSet")
        }
        if !Attachment.attachmentID.isRemoved {
            Attachment.attachmentID = TravelexpenseServiceMetadata.EntityTypes.attachment.property(withName: "AttachmentID")
        }
        if !Attachment.expenseID.isRemoved {
            Attachment.expenseID = TravelexpenseServiceMetadata.EntityTypes.attachment.property(withName: "ExpenseID")
        }
        if !Attachment.image.isRemoved {
            Attachment.image = TravelexpenseServiceMetadata.EntityTypes.attachment.property(withName: "Image")
        }
        if !Attachment.expense.isRemoved {
            Attachment.expense = TravelexpenseServiceMetadata.EntityTypes.attachment.property(withName: "Expense")
        }
        if !Currency.abbreviation.isRemoved {
            Currency.abbreviation = TravelexpenseServiceMetadata.EntityTypes.currency.property(withName: "Abbreviation")
        }
        if !Currency.currencyID.isRemoved {
            Currency.currencyID = TravelexpenseServiceMetadata.EntityTypes.currency.property(withName: "CurrencyID")
        }
        if !Currency.description.isRemoved {
            Currency.description = TravelexpenseServiceMetadata.EntityTypes.currency.property(withName: "Description")
        }
        if !Currency.expenses.isRemoved {
            Currency.expenses = TravelexpenseServiceMetadata.EntityTypes.currency.property(withName: "Expenses")
        }
        if !Expense.amount.isRemoved {
            Expense.amount = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Amount")
        }
        if !Expense.city.isRemoved {
            Expense.city = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "City")
        }
        if !Expense.country.isRemoved {
            Expense.country = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Country")
        }
        if !Expense.currencyID.isRemoved {
            Expense.currencyID = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "CurrencyID")
        }
        if !Expense.date.isRemoved {
            Expense.date = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Date")
        }
        if !Expense.expenseID.isRemoved {
            Expense.expenseID = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "ExpenseID")
        }
        if !Expense.expenseTypeID.isRemoved {
            Expense.expenseTypeID = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "ExpenseTypeID")
        }
        if !Expense.notes.isRemoved {
            Expense.notes = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Notes")
        }
        if !Expense.paymentTypeID.isRemoved {
            Expense.paymentTypeID = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "PaymentTypeID")
        }
        if !Expense.personal.isRemoved {
            Expense.personal = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Personal")
        }
        if !Expense.postalCode.isRemoved {
            Expense.postalCode = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "PostalCode")
        }
        if !Expense.reportID.isRemoved {
            Expense.reportID = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "ReportID")
        }
        if !Expense.state.isRemoved {
            Expense.state = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "State")
        }
        if !Expense.attachments.isRemoved {
            Expense.attachments = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Attachments")
        }
        if !Expense.currency.isRemoved {
            Expense.currency = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Currency")
        }
        if !Expense.expenseType.isRemoved {
            Expense.expenseType = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "ExpenseType")
        }
        if !Expense.paymentType.isRemoved {
            Expense.paymentType = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "PaymentType")
        }
        if !Expense.report.isRemoved {
            Expense.report = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Report")
        }
        if !ExpenseType.description.isRemoved {
            ExpenseType.description = TravelexpenseServiceMetadata.EntityTypes.expenseType.property(withName: "Description")
        }
        if !ExpenseType.expenseTypeID.isRemoved {
            ExpenseType.expenseTypeID = TravelexpenseServiceMetadata.EntityTypes.expenseType.property(withName: "ExpenseTypeID")
        }
        if !ExpenseType.expenses.isRemoved {
            ExpenseType.expenses = TravelexpenseServiceMetadata.EntityTypes.expenseType.property(withName: "Expenses")
        }
        if !PaymentType.description.isRemoved {
            PaymentType.description = TravelexpenseServiceMetadata.EntityTypes.paymentType.property(withName: "Description")
        }
        if !PaymentType.paymentTypeID.isRemoved {
            PaymentType.paymentTypeID = TravelexpenseServiceMetadata.EntityTypes.paymentType.property(withName: "PaymentTypeID")
        }
        if !PaymentType.expenses.isRemoved {
            PaymentType.expenses = TravelexpenseServiceMetadata.EntityTypes.paymentType.property(withName: "Expenses")
        }
        if !Report.city.isRemoved {
            Report.city = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "City")
        }
        if !Report.costCenter.isRemoved {
            Report.costCenter = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "CostCenter")
        }
        if !Report.country.isRemoved {
            Report.country = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "Country")
        }
        if !Report.end.isRemoved {
            Report.end = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "End")
        }
        if !Report.name.isRemoved {
            Report.name = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "Name")
        }
        if !Report.reportID.isRemoved {
            Report.reportID = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "ReportID")
        }
        if !Report.reportStatusID.isRemoved {
            Report.reportStatusID = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "ReportStatusID")
        }
        if !Report.start.isRemoved {
            Report.start = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "Start")
        }
        if !Report.state.isRemoved {
            Report.state = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "State")
        }
        if !Report.expenses.isRemoved {
            Report.expenses = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "Expenses")
        }
        if !Report.reportStatus.isRemoved {
            Report.reportStatus = TravelexpenseServiceMetadata.EntityTypes.report.property(withName: "ReportStatus")
        }
        if !ReportStatus.description.isRemoved {
            ReportStatus.description = TravelexpenseServiceMetadata.EntityTypes.reportStatus.property(withName: "Description")
        }
        if !ReportStatus.reportStatusID.isRemoved {
            ReportStatus.reportStatusID = TravelexpenseServiceMetadata.EntityTypes.reportStatus.property(withName: "ReportStatusID")
        }
        if !ReportStatus.reports.isRemoved {
            ReportStatus.reports = TravelexpenseServiceMetadata.EntityTypes.reportStatus.property(withName: "Reports")
        }
    }
}
