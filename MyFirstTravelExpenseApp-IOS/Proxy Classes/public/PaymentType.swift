// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

open class PaymentType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var description_: Property = TravelexpenseServiceMetadata.EntityTypes.paymentType.property(withName: "Description")

    private static var paymentTypeID_: Property = TravelexpenseServiceMetadata.EntityTypes.paymentType.property(withName: "PaymentTypeID")

    private static var expenses_: Property = TravelexpenseServiceMetadata.EntityTypes.paymentType.property(withName: "Expenses")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: TravelexpenseServiceMetadata.EntityTypes.paymentType)
    }

    open class func array(from: EntityValueList) -> [PaymentType] {
        return ArrayConverter.convert(from.toArray(), [PaymentType]())
    }

    open func copy() -> PaymentType {
        return CastRequired<PaymentType>.from(self.copyEntity())
    }

    open class var description: Property {
        get {
            objc_sync_enter(PaymentType.self)
            defer { objc_sync_exit(PaymentType.self) }
            do {
                return PaymentType.description_
            }
        }
        set(value) {
            objc_sync_enter(PaymentType.self)
            defer { objc_sync_exit(PaymentType.self) }
            do {
                PaymentType.description_ = value
            }
        }
    }

    open var description: String? {
        get {
            return StringValue.optional(self.optionalValue(for: PaymentType.description))
        }
        set(value) {
            self.setOptionalValue(for: PaymentType.description, to: StringValue.of(optional: value))
        }
    }

    open class var expenses: Property {
        get {
            objc_sync_enter(PaymentType.self)
            defer { objc_sync_exit(PaymentType.self) }
            do {
                return PaymentType.expenses_
            }
        }
        set(value) {
            objc_sync_enter(PaymentType.self)
            defer { objc_sync_exit(PaymentType.self) }
            do {
                PaymentType.expenses_ = value
            }
        }
    }

    open var expenses: [Expense] {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: PaymentType.expenses)).toArray(), [Expense]())
        }
        set(value) {
            PaymentType.expenses.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(paymentTypeID: Int64?) -> EntityKey {
        return EntityKey().with(name: "PaymentTypeID", value: LongValue.of(optional: paymentTypeID))
    }

    open var old: PaymentType {
        return CastRequired<PaymentType>.from(self.oldEntity)
    }

    open class var paymentTypeID: Property {
        get {
            objc_sync_enter(PaymentType.self)
            defer { objc_sync_exit(PaymentType.self) }
            do {
                return PaymentType.paymentTypeID_
            }
        }
        set(value) {
            objc_sync_enter(PaymentType.self)
            defer { objc_sync_exit(PaymentType.self) }
            do {
                PaymentType.paymentTypeID_ = value
            }
        }
    }

    open var paymentTypeID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: PaymentType.paymentTypeID))
        }
        set(value) {
            self.setOptionalValue(for: PaymentType.paymentTypeID, to: LongValue.of(optional: value))
        }
    }
}
