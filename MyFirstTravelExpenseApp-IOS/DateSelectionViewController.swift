//
//  DateSelectionViewController.swift
//  MyFirstTravelExpenseApp-IOS
//
//  Created by agnair on 18/9/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit
import SAPFiori


protocol DateSelectionViewControllerProtocol {

    func didSelectDate(_ date: Date)
}


class DateSelectionViewController: UIViewController {

    var delegate: DateSelectionViewControllerProtocol?

    override func viewDidLoad() {

        super.viewDidLoad()

        let calendarView = FUICalendarView(calendarStyle: .monthView, weekStartDay: .sunday)

        calendarView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(calendarView)

        NSLayoutConstraint.activate([

            NSLayoutConstraint(item: calendarView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0),

            NSLayoutConstraint(item: calendarView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0),

            NSLayoutConstraint(item: calendarView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)])

        calendarView.delegate = self

    }

}



extension DateSelectionViewController: FUICalendarViewDelegate {

    func calendarView(_ calendarView: FUICalendarView, didChangeSelections selections: [FUIDateSelection]) {}



    func calendarView(_ calendarView: FUICalendarView, didChangeVisibleDatesTo visibleDates: FUIVisibleDates) {}



    func calendarView(_ calendarView: FUICalendarView, didSelectDate date: Date, cell: FUICalendarItemCollectionViewCell) {

        delegate?.didSelectDate(date)

        self.navigationController?.popViewController(animated: true)
    }



    func calendarView(_ calendarView: FUICalendarView,didDeselectDate date: Date, cell: FUICalendarItemCollectionViewCell) {}



    func calendar(_ calendarView: FUICalendarView, willDisplay cell: FUICalendarItemCollectionViewCell, forItemAt date: Date, indexPath: IndexPath) {}



    func calendarView(_ calendarView: FUICalendarView, didChangeTitleTo title: String) {


    }

}
