//
//  ReportsDetailViewController.swift
//  MyFirstTravelExpenseApp-IOS
//
//  Created by ponkar on 18/9/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit
import SAPOData
import SAPOfflineOData
import SAPFiori

class ReportsDetailViewController: UIViewController {
    
    @IBOutlet weak var tview: UITableView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var submitButton: FUIButton!
    
    let imagePicker = UIImagePickerController()
    
    var selectedExpense: Expense?
    
    @IBOutlet weak var statusLabel: UILabel!
    var total : [String: Double]? {
        didSet {
            var text = ""
            for (i,t) in total!.enumerated() {
                text += t.key + " " + String(describing: t.value)
                if (i + 1) < total!.count {
                    text += ", "
                }
            }
    
            totalLabel.text = text
        }
    }
    
    var travelexpenseService: TravelexpenseService<OfflineODataProvider>?
    var expenses = [Expense]()
    
    var report : Report? {
        didSet {
            self.title = report?.name ?? ""
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

         tview.register(FUIObjectTableViewCell.self, forCellReuseIdentifier: FUIObjectTableViewCell.reuseIdentifier)
        tview.delegate = self
        tview.dataSource = self
        
        submitButton.style = .fuiRoundedFilled
        submitButton.isUserInteractionEnabled = true
        
        self.statusLabel.text = report?.reportStatus?.description ?? ""
        submitButton.isEnabled = (report?.reportStatusID == 4)
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(submitReport))
        tapGesture.numberOfTapsRequired = 1
        submitButton.addGestureRecognizer(tapGesture)
        
        imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.sourceType = .photoLibrary
        
        loadData()
    }
    
    @objc func submitReport() {

        report?.reportStatusID = 2
        
        travelexpenseService?.updateEntity(report!) { [weak self] error in
            // Make sure no errors occurred.
            if let error = error {
                NSLog("Error: %@", error.localizedDescription)
            }
            
            self?.statusLabel.text = "In Review"
        }
        
        AlertHelper.displayAlert(with: "Report Submitted. Good luck!", error: nil, viewController: self)
        
    }

    private func loadData() {
        
        let reportID = report?.reportID!
        let query = DataQuery().filter(Expense.reportID == reportID!)
            .expand(Expense.expenseType, Expense.attachments, Expense.currency)
            .orderBy(Expense.date)
        
        // Fetch all expense items, using the query parameters defined above.
        travelexpenseService?.fetchExpenseSet(matching: query) { [weak self] expenses, error in
            // Make sure no errors occurred.
            if let error = error {
                NSLog("Error: %@", error.localizedDescription)
                return
            }
            self?.expenses = expenses!
            self?.updateTotal()
            self?.tview.reloadData()
        }
    }
    
    private func updateTotal() {
        
        var totalAmount = [String : Double]()
        for expense in expenses {
            guard let description = expense.currency?.abbreviation else { continue }
            totalAmount[description] = (totalAmount[description] ?? 0.0) + (expense.amount?.doubleValue() ?? 0.0)
        }
        
       total = totalAmount
    }
}

extension ReportsDetailViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expenses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let expense = expenses[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: FUIObjectTableViewCell.reuseIdentifier) as! FUIObjectTableViewCell
        cell.headlineText = expense.city!
        cell.subheadlineText = expense.country
        cell.footnoteText = expense.date?.toString()
        cell.statusText = (expense.currency?.abbreviation ?? "") + " " + (expense.amount?.toString() ?? "")
        
        if expense.attachments.count > 0 {
            cell.iconImages = [FUIIconLibrary.indicator.attachment.withRenderingMode(.alwaysTemplate)]
        }
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteTitle = NSLocalizedString("Delete", comment: "Delete action")
        let deleteAction = UITableViewRowAction(style: .destructive,
                                                title: deleteTitle) { (action, indexPath) in
                                                    let existingExpense = self.expenses[indexPath.row]
        self.travelexpenseService?.deleteEntity(existingExpense) {[weak self] error in
            // Make sure no errors occurred.
            if let error = error {
                NSLog("Error: %@", error.localizedDescription)
            }
            self?.tview.reloadData()
            }
        }
        
        let attachTitle = NSLocalizedString("Receipt", comment: "Attach Receipt")
        let attachAction = UITableViewRowAction(style: .normal,
                                                  title: attachTitle) { (action, indexPath) in
                        self.selectedExpense = self.expenses[indexPath.row]
                                                    self.present(self.imagePicker, animated: true, completion: nil)
        }
        attachAction.backgroundColor = .green
        return [attachAction, deleteAction]
    }
    
}

extension ReportsDetailViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
            let imageData = pickedImage.pngData()
            let attachment = Attachment(withDefaults: false)
            attachment.image = imageData
            
            self.selectedExpense?.attachments = [attachment]
            
            self.travelexpenseService?.createEntity(self.selectedExpense!) {[weak self] error in
                if let error = error {
                    AlertHelper.displayAlert(with: "Could not create Expense", error: error, viewController: self!)
                }
                self?.tview.reloadData()
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
}
