// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

open class Expense: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var amount_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Amount")

    private static var city_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "City")

    private static var country_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Country")

    private static var currencyID_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "CurrencyID")

    private static var date_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Date")

    private static var expenseID_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "ExpenseID")

    private static var expenseTypeID_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "ExpenseTypeID")

    private static var notes_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Notes")

    private static var paymentTypeID_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "PaymentTypeID")

    private static var personal_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Personal")

    private static var postalCode_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "PostalCode")

    private static var reportID_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "ReportID")

    private static var state_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "State")

    private static var attachments_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Attachments")

    private static var currency_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Currency")

    private static var expenseType_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "ExpenseType")

    private static var paymentType_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "PaymentType")

    private static var report_: Property = TravelexpenseServiceMetadata.EntityTypes.expense.property(withName: "Report")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: TravelexpenseServiceMetadata.EntityTypes.expense)
    }

    open class var amount: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.amount_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.amount_ = value
            }
        }
    }

    open var amount: BigDecimal? {
        get {
            return DecimalValue.optional(self.optionalValue(for: Expense.amount))
        }
        set(value) {
            self.setOptionalValue(for: Expense.amount, to: DecimalValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [Expense] {
        return ArrayConverter.convert(from.toArray(), [Expense]())
    }

    open class var attachments: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.attachments_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.attachments_ = value
            }
        }
    }

    open var attachments: [Attachment] {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: Expense.attachments)).toArray(), [Attachment]())
        }
        set(value) {
            Expense.attachments.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open class var city: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.city_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.city_ = value
            }
        }
    }

    open var city: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Expense.city))
        }
        set(value) {
            self.setOptionalValue(for: Expense.city, to: StringValue.of(optional: value))
        }
    }

    open func copy() -> Expense {
        return CastRequired<Expense>.from(self.copyEntity())
    }

    open class var country: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.country_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.country_ = value
            }
        }
    }

    open var country: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Expense.country))
        }
        set(value) {
            self.setOptionalValue(for: Expense.country, to: StringValue.of(optional: value))
        }
    }

    open class var currency: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.currency_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.currency_ = value
            }
        }
    }

    open var currency: Currency? {
        get {
            return CastOptional<Currency>.from(self.optionalValue(for: Expense.currency))
        }
        set(value) {
            self.setOptionalValue(for: Expense.currency, to: value)
        }
    }

    open class var currencyID: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.currencyID_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.currencyID_ = value
            }
        }
    }

    open var currencyID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: Expense.currencyID))
        }
        set(value) {
            self.setOptionalValue(for: Expense.currencyID, to: LongValue.of(optional: value))
        }
    }

    open class var date: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.date_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.date_ = value
            }
        }
    }

    open var date: LocalDateTime? {
        get {
            return LocalDateTime.castOptional(self.optionalValue(for: Expense.date))
        }
        set(value) {
            self.setOptionalValue(for: Expense.date, to: value)
        }
    }

    open class var expenseID: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.expenseID_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.expenseID_ = value
            }
        }
    }

    open var expenseID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: Expense.expenseID))
        }
        set(value) {
            self.setOptionalValue(for: Expense.expenseID, to: LongValue.of(optional: value))
        }
    }

    open class var expenseType: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.expenseType_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.expenseType_ = value
            }
        }
    }

    open var expenseType: ExpenseType? {
        get {
            return CastOptional<ExpenseType>.from(self.optionalValue(for: Expense.expenseType))
        }
        set(value) {
            self.setOptionalValue(for: Expense.expenseType, to: value)
        }
    }

    open class var expenseTypeID: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.expenseTypeID_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.expenseTypeID_ = value
            }
        }
    }

    open var expenseTypeID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: Expense.expenseTypeID))
        }
        set(value) {
            self.setOptionalValue(for: Expense.expenseTypeID, to: LongValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(expenseID: Int64?) -> EntityKey {
        return EntityKey().with(name: "ExpenseID", value: LongValue.of(optional: expenseID))
    }

    open class var notes: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.notes_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.notes_ = value
            }
        }
    }

    open var notes: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Expense.notes))
        }
        set(value) {
            self.setOptionalValue(for: Expense.notes, to: StringValue.of(optional: value))
        }
    }

    open var old: Expense {
        return CastRequired<Expense>.from(self.oldEntity)
    }

    open class var paymentType: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.paymentType_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.paymentType_ = value
            }
        }
    }

    open var paymentType: PaymentType? {
        get {
            return CastOptional<PaymentType>.from(self.optionalValue(for: Expense.paymentType))
        }
        set(value) {
            self.setOptionalValue(for: Expense.paymentType, to: value)
        }
    }

    open class var paymentTypeID: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.paymentTypeID_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.paymentTypeID_ = value
            }
        }
    }

    open var paymentTypeID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: Expense.paymentTypeID))
        }
        set(value) {
            self.setOptionalValue(for: Expense.paymentTypeID, to: LongValue.of(optional: value))
        }
    }

    open class var personal: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.personal_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.personal_ = value
            }
        }
    }

    open var personal: Bool? {
        get {
            return BooleanValue.optional(self.optionalValue(for: Expense.personal))
        }
        set(value) {
            self.setOptionalValue(for: Expense.personal, to: BooleanValue.of(optional: value))
        }
    }

    open class var postalCode: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.postalCode_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.postalCode_ = value
            }
        }
    }

    open var postalCode: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Expense.postalCode))
        }
        set(value) {
            self.setOptionalValue(for: Expense.postalCode, to: StringValue.of(optional: value))
        }
    }

    open class var report: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.report_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.report_ = value
            }
        }
    }

    open var report: Report? {
        get {
            return CastOptional<Report>.from(self.optionalValue(for: Expense.report))
        }
        set(value) {
            self.setOptionalValue(for: Expense.report, to: value)
        }
    }

    open class var reportID: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.reportID_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.reportID_ = value
            }
        }
    }

    open var reportID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: Expense.reportID))
        }
        set(value) {
            self.setOptionalValue(for: Expense.reportID, to: LongValue.of(optional: value))
        }
    }

    open class var state: Property {
        get {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                return Expense.state_
            }
        }
        set(value) {
            objc_sync_enter(Expense.self)
            defer { objc_sync_exit(Expense.self) }
            do {
                Expense.state_ = value
            }
        }
    }

    open var state: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Expense.state))
        }
        set(value) {
            self.setOptionalValue(for: Expense.state, to: StringValue.of(optional: value))
        }
    }
}
