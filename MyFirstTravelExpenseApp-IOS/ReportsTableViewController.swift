//
//  ReportsTableViewController.swift
//  MyFirstTravelExpenseApp-IOS
//
//  Created by agnair on 18/9/19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit
import SAPOData
import SAPOfflineOData
import SAPFiori

class ReportsTableViewController: UITableViewController {

    var travelexpenseService: TravelexpenseService<OfflineODataProvider>?
    var reports = [Report]()

    @IBOutlet weak var createBarbutton: UIBarButtonItem!


    @IBAction func createReport(_ sender: UIBarButtonItem) {

        let reportDetailVC = UIStoryboard.init(name: "SubScreens", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewReportTableViewController") as! NewReportTableViewController

        let navVC = UINavigationController.init(rootViewController: reportDetailVC)
        self.present(navVC, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "My Expenses"
        createBarbutton.image = FUIIconLibrary.system.create

        tableView.backgroundColor = UIColor.preferredFioriColor(forStyle: .backgroundBase)

        tableView.register(FUIObjectTableViewCell.self, forCellReuseIdentifier: FUIObjectTableViewCell.reuseIdentifier)
        setupKPIHeader()

        guard let travelexpenseService = appDelegate.sessionManager.onboardingSession?.odataController.travelexpenseService else {
            AlertHelper.displayAlert(with: "OData service is not reachable, please onboard again.", error: nil, viewController: self)
            return
        }
        self.travelexpenseService = travelexpenseService

        loadData()
    }

    private func loadData() {
        let query = DataQuery().orderBy(Report.end, .descending).expand(Report.reportStatus)

        // Fetch all expense reports, using the query parameters defined above.
        travelexpenseService?.fetchReportSet(matching: query) { [weak self] reports, error in
            if let error = error {
                NSLog("Error: %@", error.localizedDescription)
                return
            }
            self?.reports = reports!
            self?.setupKPIHeader()
            self?.tableView.reloadSections(NSIndexSet.init(index: 0) as IndexSet, with: .automatic)
        }
    }
    

    private func setupKPIHeader() {

        let kpiHeader = FUIKPIHeader()

        var kpiHeaderDict = [String : Int]()
        
        for report in reports {
            
            guard let description = report.reportStatus?.description else { return }
            kpiHeaderDict[description] =  (kpiHeaderDict[description] ?? 0) + 1
            
            let standardKPI1 = FUIKPIView()
            let count = String(describing: kpiHeaderDict[description]!)
            standardKPI1.items = [FUIKPIMetricItem(string: count)]
            standardKPI1.captionlabel.text = description
            standardKPI1.colorScheme = .dark
            standardKPI1.isEnabled = true
            kpiHeader.items.append(standardKPI1)
        }

        tableView.tableHeaderView = kpiHeader
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return 0 as we display only the header
        return reports.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let report = reports[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: FUIObjectTableViewCell.reuseIdentifier) as! FUIObjectTableViewCell
        cell.headlineText = report.name
        cell.subheadlineText = report.city
        cell.footnoteText = report.start?.toString()
        cell.statusText = report.reportStatus?.description

        var colorStyle = FUIColorStyle.positive

        switch report.reportStatus?.description {
        case "Approved":
            colorStyle = .positive
        case "Declined":
            colorStyle = .negative
        case "Open":
            colorStyle = .critical
        case "In Review":
            colorStyle = .critical
        case .none,.some(_):
            break
        }


        cell.statusLabel.textColor = .preferredFioriColor(forStyle: colorStyle)
        
        cell.accessoryType = .disclosureIndicator

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let reportDetailVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "reportDetailVC") as! ReportsDetailViewController
        reportDetailVC.report = reports[indexPath.row]
        reportDetailVC.travelexpenseService = self.travelexpenseService!
        self.navigationController?.pushViewController(reportDetailVC, animated: true)
    }
    

}

extension UIViewController {
    var appDelegate: AppDelegate { return (UIApplication.shared.delegate as! AppDelegate) }
}
