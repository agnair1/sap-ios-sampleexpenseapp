// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

open class ExpenseType: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var description_: Property = TravelexpenseServiceMetadata.EntityTypes.expenseType.property(withName: "Description")

    private static var expenseTypeID_: Property = TravelexpenseServiceMetadata.EntityTypes.expenseType.property(withName: "ExpenseTypeID")

    private static var expenses_: Property = TravelexpenseServiceMetadata.EntityTypes.expenseType.property(withName: "Expenses")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: TravelexpenseServiceMetadata.EntityTypes.expenseType)
    }

    open class func array(from: EntityValueList) -> [ExpenseType] {
        return ArrayConverter.convert(from.toArray(), [ExpenseType]())
    }

    open func copy() -> ExpenseType {
        return CastRequired<ExpenseType>.from(self.copyEntity())
    }

    open class var description: Property {
        get {
            objc_sync_enter(ExpenseType.self)
            defer { objc_sync_exit(ExpenseType.self) }
            do {
                return ExpenseType.description_
            }
        }
        set(value) {
            objc_sync_enter(ExpenseType.self)
            defer { objc_sync_exit(ExpenseType.self) }
            do {
                ExpenseType.description_ = value
            }
        }
    }

    open var description: String? {
        get {
            return StringValue.optional(self.optionalValue(for: ExpenseType.description))
        }
        set(value) {
            self.setOptionalValue(for: ExpenseType.description, to: StringValue.of(optional: value))
        }
    }

    open class var expenseTypeID: Property {
        get {
            objc_sync_enter(ExpenseType.self)
            defer { objc_sync_exit(ExpenseType.self) }
            do {
                return ExpenseType.expenseTypeID_
            }
        }
        set(value) {
            objc_sync_enter(ExpenseType.self)
            defer { objc_sync_exit(ExpenseType.self) }
            do {
                ExpenseType.expenseTypeID_ = value
            }
        }
    }

    open var expenseTypeID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: ExpenseType.expenseTypeID))
        }
        set(value) {
            self.setOptionalValue(for: ExpenseType.expenseTypeID, to: LongValue.of(optional: value))
        }
    }

    open class var expenses: Property {
        get {
            objc_sync_enter(ExpenseType.self)
            defer { objc_sync_exit(ExpenseType.self) }
            do {
                return ExpenseType.expenses_
            }
        }
        set(value) {
            objc_sync_enter(ExpenseType.self)
            defer { objc_sync_exit(ExpenseType.self) }
            do {
                ExpenseType.expenses_ = value
            }
        }
    }

    open var expenses: [Expense] {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: ExpenseType.expenses)).toArray(), [Expense]())
        }
        set(value) {
            ExpenseType.expenses.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(expenseTypeID: Int64?) -> EntityKey {
        return EntityKey().with(name: "ExpenseTypeID", value: LongValue.of(optional: expenseTypeID))
    }

    open var old: ExpenseType {
        return CastRequired<ExpenseType>.from(self.oldEntity)
    }
}
