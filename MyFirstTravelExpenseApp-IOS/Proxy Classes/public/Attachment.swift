// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

open class Attachment: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var attachmentID_: Property = TravelexpenseServiceMetadata.EntityTypes.attachment.property(withName: "AttachmentID")

    private static var expenseID_: Property = TravelexpenseServiceMetadata.EntityTypes.attachment.property(withName: "ExpenseID")

    private static var image_: Property = TravelexpenseServiceMetadata.EntityTypes.attachment.property(withName: "Image")

    private static var expense_: Property = TravelexpenseServiceMetadata.EntityTypes.attachment.property(withName: "Expense")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: TravelexpenseServiceMetadata.EntityTypes.attachment)
    }

    open class func array(from: EntityValueList) -> [Attachment] {
        return ArrayConverter.convert(from.toArray(), [Attachment]())
    }

    open class var attachmentID: Property {
        get {
            objc_sync_enter(Attachment.self)
            defer { objc_sync_exit(Attachment.self) }
            do {
                return Attachment.attachmentID_
            }
        }
        set(value) {
            objc_sync_enter(Attachment.self)
            defer { objc_sync_exit(Attachment.self) }
            do {
                Attachment.attachmentID_ = value
            }
        }
    }

    open var attachmentID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: Attachment.attachmentID))
        }
        set(value) {
            self.setOptionalValue(for: Attachment.attachmentID, to: LongValue.of(optional: value))
        }
    }

    open func copy() -> Attachment {
        return CastRequired<Attachment>.from(self.copyEntity())
    }

    open class var expense: Property {
        get {
            objc_sync_enter(Attachment.self)
            defer { objc_sync_exit(Attachment.self) }
            do {
                return Attachment.expense_
            }
        }
        set(value) {
            objc_sync_enter(Attachment.self)
            defer { objc_sync_exit(Attachment.self) }
            do {
                Attachment.expense_ = value
            }
        }
    }

    open var expense: Expense? {
        get {
            return CastOptional<Expense>.from(self.optionalValue(for: Attachment.expense))
        }
        set(value) {
            self.setOptionalValue(for: Attachment.expense, to: value)
        }
    }

    open class var expenseID: Property {
        get {
            objc_sync_enter(Attachment.self)
            defer { objc_sync_exit(Attachment.self) }
            do {
                return Attachment.expenseID_
            }
        }
        set(value) {
            objc_sync_enter(Attachment.self)
            defer { objc_sync_exit(Attachment.self) }
            do {
                Attachment.expenseID_ = value
            }
        }
    }

    open var expenseID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: Attachment.expenseID))
        }
        set(value) {
            self.setOptionalValue(for: Attachment.expenseID, to: LongValue.of(optional: value))
        }
    }

    open class var image: Property {
        get {
            objc_sync_enter(Attachment.self)
            defer { objc_sync_exit(Attachment.self) }
            do {
                return Attachment.image_
            }
        }
        set(value) {
            objc_sync_enter(Attachment.self)
            defer { objc_sync_exit(Attachment.self) }
            do {
                Attachment.image_ = value
            }
        }
    }

    open var image: Data? {
        get {
            return BinaryValue.optional(self.optionalValue(for: Attachment.image))
        }
        set(value) {
            self.setOptionalValue(for: Attachment.image, to: BinaryValue.of(optional: value))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(attachmentID: Int64?) -> EntityKey {
        return EntityKey().with(name: "AttachmentID", value: LongValue.of(optional: attachmentID))
    }

    open var old: Attachment {
        return CastRequired<Attachment>.from(self.oldEntity)
    }
}
