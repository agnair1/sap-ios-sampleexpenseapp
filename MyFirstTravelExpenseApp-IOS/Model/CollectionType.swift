//
// MyFirstTravelExpenseApp-IOS
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 18/09/19
//

import Foundation

enum CollectionType: String {
    case currencySet = "CurrencySet"
    case expenseSet = "ExpenseSet"
    case reportSet = "ReportSet"
    case expenseTypeSet = "ExpenseTypeSet"
    case attachmentSet = "AttachmentSet"
    case reportStatusSet = "ReportStatusSet"
    case paymentTypeSet = "PaymentTypeSet"
    case none = ""
    static let all = [currencySet, expenseSet, reportSet, expenseTypeSet, attachmentSet, reportStatusSet, paymentTypeSet]
}
