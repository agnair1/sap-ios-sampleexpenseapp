// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

open class Currency: EntityValue {
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private static var abbreviation_: Property = TravelexpenseServiceMetadata.EntityTypes.currency.property(withName: "Abbreviation")

    private static var currencyID_: Property = TravelexpenseServiceMetadata.EntityTypes.currency.property(withName: "CurrencyID")

    private static var description_: Property = TravelexpenseServiceMetadata.EntityTypes.currency.property(withName: "Description")

    private static var expenses_: Property = TravelexpenseServiceMetadata.EntityTypes.currency.property(withName: "Expenses")

    public init(withDefaults: Bool = true) {
        super.init(withDefaults: withDefaults, type: TravelexpenseServiceMetadata.EntityTypes.currency)
    }

    open class var abbreviation: Property {
        get {
            objc_sync_enter(Currency.self)
            defer { objc_sync_exit(Currency.self) }
            do {
                return Currency.abbreviation_
            }
        }
        set(value) {
            objc_sync_enter(Currency.self)
            defer { objc_sync_exit(Currency.self) }
            do {
                Currency.abbreviation_ = value
            }
        }
    }

    open var abbreviation: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Currency.abbreviation))
        }
        set(value) {
            self.setOptionalValue(for: Currency.abbreviation, to: StringValue.of(optional: value))
        }
    }

    open class func array(from: EntityValueList) -> [Currency] {
        return ArrayConverter.convert(from.toArray(), [Currency]())
    }

    open func copy() -> Currency {
        return CastRequired<Currency>.from(self.copyEntity())
    }

    open class var currencyID: Property {
        get {
            objc_sync_enter(Currency.self)
            defer { objc_sync_exit(Currency.self) }
            do {
                return Currency.currencyID_
            }
        }
        set(value) {
            objc_sync_enter(Currency.self)
            defer { objc_sync_exit(Currency.self) }
            do {
                Currency.currencyID_ = value
            }
        }
    }

    open var currencyID: Int64? {
        get {
            return LongValue.optional(self.optionalValue(for: Currency.currencyID))
        }
        set(value) {
            self.setOptionalValue(for: Currency.currencyID, to: LongValue.of(optional: value))
        }
    }

    open class var description: Property {
        get {
            objc_sync_enter(Currency.self)
            defer { objc_sync_exit(Currency.self) }
            do {
                return Currency.description_
            }
        }
        set(value) {
            objc_sync_enter(Currency.self)
            defer { objc_sync_exit(Currency.self) }
            do {
                Currency.description_ = value
            }
        }
    }

    open var description: String? {
        get {
            return StringValue.optional(self.optionalValue(for: Currency.description))
        }
        set(value) {
            self.setOptionalValue(for: Currency.description, to: StringValue.of(optional: value))
        }
    }

    open class var expenses: Property {
        get {
            objc_sync_enter(Currency.self)
            defer { objc_sync_exit(Currency.self) }
            do {
                return Currency.expenses_
            }
        }
        set(value) {
            objc_sync_enter(Currency.self)
            defer { objc_sync_exit(Currency.self) }
            do {
                Currency.expenses_ = value
            }
        }
    }

    open var expenses: [Expense] {
        get {
            return ArrayConverter.convert(EntityValueList.castRequired(self.requiredValue(for: Currency.expenses)).toArray(), [Expense]())
        }
        set(value) {
            Currency.expenses.setEntityList(in: self, to: EntityValueList.fromArray(ArrayConverter.convert(value, [EntityValue]())))
        }
    }

    open override var isProxy: Bool {
        return true
    }

    open class func key(currencyID: Int64?) -> EntityKey {
        return EntityKey().with(name: "CurrencyID", value: LongValue.of(optional: currencyID))
    }

    open var old: Currency {
        return CastRequired<Currency>.from(self.oldEntity)
    }
}
