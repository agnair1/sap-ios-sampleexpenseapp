// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

public class TravelexpenseServiceMetadata {
    private static var document_: CSDLDocument = TravelexpenseServiceMetadata.resolve()

    public static var document: CSDLDocument {
        get {
            objc_sync_enter(TravelexpenseServiceMetadata.self)
            defer { objc_sync_exit(TravelexpenseServiceMetadata.self) }
            do {
                return TravelexpenseServiceMetadata.document_
            }
        }
        set(value) {
            objc_sync_enter(TravelexpenseServiceMetadata.self)
            defer { objc_sync_exit(TravelexpenseServiceMetadata.self) }
            do {
                TravelexpenseServiceMetadata.document_ = value
            }
        }
    }

    private static func resolve() -> CSDLDocument {
        try! TravelexpenseServiceFactory.registerAll()
        TravelexpenseServiceMetadataParser.parsed.hasGeneratedProxies = true
        return TravelexpenseServiceMetadataParser.parsed
    }

    public class EntityTypes {
        private static var attachment_: EntityType = TravelexpenseServiceMetadataParser.parsed.entityType(withName: "com.example.backend.travelexpense.Attachment")

        private static var currency_: EntityType = TravelexpenseServiceMetadataParser.parsed.entityType(withName: "com.example.backend.travelexpense.Currency")

        private static var expense_: EntityType = TravelexpenseServiceMetadataParser.parsed.entityType(withName: "com.example.backend.travelexpense.Expense")

        private static var expenseType_: EntityType = TravelexpenseServiceMetadataParser.parsed.entityType(withName: "com.example.backend.travelexpense.ExpenseType")

        private static var paymentType_: EntityType = TravelexpenseServiceMetadataParser.parsed.entityType(withName: "com.example.backend.travelexpense.PaymentType")

        private static var report_: EntityType = TravelexpenseServiceMetadataParser.parsed.entityType(withName: "com.example.backend.travelexpense.Report")

        private static var reportStatus_: EntityType = TravelexpenseServiceMetadataParser.parsed.entityType(withName: "com.example.backend.travelexpense.ReportStatus")

        public static var attachment: EntityType {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    return TravelexpenseServiceMetadata.EntityTypes.attachment_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    TravelexpenseServiceMetadata.EntityTypes.attachment_ = value
                }
            }
        }

        public static var currency: EntityType {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    return TravelexpenseServiceMetadata.EntityTypes.currency_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    TravelexpenseServiceMetadata.EntityTypes.currency_ = value
                }
            }
        }

        public static var expense: EntityType {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    return TravelexpenseServiceMetadata.EntityTypes.expense_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    TravelexpenseServiceMetadata.EntityTypes.expense_ = value
                }
            }
        }

        public static var expenseType: EntityType {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    return TravelexpenseServiceMetadata.EntityTypes.expenseType_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    TravelexpenseServiceMetadata.EntityTypes.expenseType_ = value
                }
            }
        }

        public static var paymentType: EntityType {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    return TravelexpenseServiceMetadata.EntityTypes.paymentType_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    TravelexpenseServiceMetadata.EntityTypes.paymentType_ = value
                }
            }
        }

        public static var report: EntityType {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    return TravelexpenseServiceMetadata.EntityTypes.report_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    TravelexpenseServiceMetadata.EntityTypes.report_ = value
                }
            }
        }

        public static var reportStatus: EntityType {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    return TravelexpenseServiceMetadata.EntityTypes.reportStatus_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntityTypes.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntityTypes.self) }
                do {
                    TravelexpenseServiceMetadata.EntityTypes.reportStatus_ = value
                }
            }
        }
    }

    public class EntitySets {
        private static var attachmentSet_: EntitySet = TravelexpenseServiceMetadataParser.parsed.entitySet(withName: "AttachmentSet")

        private static var currencySet_: EntitySet = TravelexpenseServiceMetadataParser.parsed.entitySet(withName: "CurrencySet")

        private static var expenseSet_: EntitySet = TravelexpenseServiceMetadataParser.parsed.entitySet(withName: "ExpenseSet")

        private static var expenseTypeSet_: EntitySet = TravelexpenseServiceMetadataParser.parsed.entitySet(withName: "ExpenseTypeSet")

        private static var paymentTypeSet_: EntitySet = TravelexpenseServiceMetadataParser.parsed.entitySet(withName: "PaymentTypeSet")

        private static var reportSet_: EntitySet = TravelexpenseServiceMetadataParser.parsed.entitySet(withName: "ReportSet")

        private static var reportStatusSet_: EntitySet = TravelexpenseServiceMetadataParser.parsed.entitySet(withName: "ReportStatusSet")

        public static var attachmentSet: EntitySet {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    return TravelexpenseServiceMetadata.EntitySets.attachmentSet_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    TravelexpenseServiceMetadata.EntitySets.attachmentSet_ = value
                }
            }
        }

        public static var currencySet: EntitySet {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    return TravelexpenseServiceMetadata.EntitySets.currencySet_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    TravelexpenseServiceMetadata.EntitySets.currencySet_ = value
                }
            }
        }

        public static var expenseSet: EntitySet {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    return TravelexpenseServiceMetadata.EntitySets.expenseSet_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    TravelexpenseServiceMetadata.EntitySets.expenseSet_ = value
                }
            }
        }

        public static var expenseTypeSet: EntitySet {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    return TravelexpenseServiceMetadata.EntitySets.expenseTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    TravelexpenseServiceMetadata.EntitySets.expenseTypeSet_ = value
                }
            }
        }

        public static var paymentTypeSet: EntitySet {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    return TravelexpenseServiceMetadata.EntitySets.paymentTypeSet_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    TravelexpenseServiceMetadata.EntitySets.paymentTypeSet_ = value
                }
            }
        }

        public static var reportSet: EntitySet {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    return TravelexpenseServiceMetadata.EntitySets.reportSet_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    TravelexpenseServiceMetadata.EntitySets.reportSet_ = value
                }
            }
        }

        public static var reportStatusSet: EntitySet {
            get {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    return TravelexpenseServiceMetadata.EntitySets.reportStatusSet_
                }
            }
            set(value) {
                objc_sync_enter(TravelexpenseServiceMetadata.EntitySets.self)
                defer { objc_sync_exit(TravelexpenseServiceMetadata.EntitySets.self) }
                do {
                    TravelexpenseServiceMetadata.EntitySets.reportStatusSet_ = value
                }
            }
        }
    }
}
